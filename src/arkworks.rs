/// # CensorProof: Small dalek-bulletproofs example
///
/// Context: Peggy wants to prove that her message does not contain a bad word.
use ark_ff::Field;
use ark_r1cs_std::prelude::*;
use ark_relations::r1cs::{ConstraintSynthesizer, ConstraintSystemRef, SynthesisError};

/// # Contains either the message text (for the Prover) or its length (for the Verifier)
pub enum MessageData {
    Text(Vec<u8>),
    Length(usize),
}

/// # Proof Synthesiser
///
/// # Fields
///
/// * msg_data: MessageData::Text for Prover, MessageData::Length for Verifier
/// * bad_word: Censored words
pub struct CensorProofSynthesizer {
    msg_data: MessageData,
    bad_word: Vec<u8>,
}

/// Implement ConstraintSynthesizer to be able to generate constraints
/// for both prover and verifier.
impl<F: Field> ConstraintSynthesizer<F> for CensorProofSynthesizer {
    /// # Generate constraints for the constraint system using this struct's Fields
    fn generate_constraints(self, cs: ConstraintSystemRef<F>) -> Result<(), SynthesisError> {
        //Allocate Text
        let msg = self.allocate_text(&cs)?;
        // Accumulator: is word found in text?
        let mut found = Boolean::constant(false);
        // Sliding window over text with size equal to word length
        for i in 0..(msg.len() - self.bad_word.len() + 1) {
            // Accumulator: does each character match?
            let mut word_match = Boolean::constant(true);
            for j in 0..self.bad_word.len() {
                // Get characters
                let c1 = &msg[i + j];
                let c2 = self.bad_word[j];
                let c2 = UInt8::constant(c2);
                // Compare characters
                let eq = c1.is_eq(&c2)?;
                // Apply AND over each character
                word_match = word_match.and(&eq)?;
            }
            // Apply OR over each window
            found = found.or(&word_match)?;
        }
        // Constrain `found` *not* true
        found.enforce_not_equal(&Boolean::constant(true))?;
        Ok(())
    }
}

impl CensorProofSynthesizer {
    /// Create a new synthesizer for a prover
    ///
    /// # Arguments
    ///
    /// * msg: Byte slice containing the message
    /// * bad_word: Byte slice containing the censored word
    pub fn new_prover(msg: &[u8], bad_word: &[u8]) -> Self {
        let msg_data = MessageData::Text(msg.to_vec());
        let bad_word = bad_word.to_vec();
        CensorProofSynthesizer { msg_data, bad_word }
    }

    /// Create a new synthesizer for a verifier
    ///
    /// # Arguments
    ///
    /// * msg_len: Length of the message
    /// * bad_word: Byte slice containing the censored word
    pub fn new_verifier(msg_len: usize, bad_word: &[u8]) -> Self {
        let msg_data = MessageData::Length(msg_len);
        let bad_word = bad_word.to_vec();
        CensorProofSynthesizer { msg_data, bad_word }
    }

    /// Allocate variables based on the MessageData
    ///
    /// # Arguments
    ///
    /// * cs: Prover or Verifier
    fn allocate_text<F: Field>(
        &self,
        cs: &ConstraintSystemRef<F>,
    ) -> Result<Vec<UInt8<F>>, SynthesisError> {
        match &self.msg_data {
            // Prover has plaintext -> allocates plaintext
            MessageData::Text(t) => t
                .iter()
                .map(|c| UInt8::new_witness(ark_relations::ns!(cs, "text"), || Ok(c)))
                .collect(),
            // Verifier does *not* have plaintext -> allocates number of
            // variables equal to the text length
            MessageData::Length(l) => (0..*l)
                .map(|_| {
                    UInt8::new_witness(ark_relations::ns!(cs, "text"), || -> Result<u8, _> {
                        // If assignment would be required we throw an error
                        Err(SynthesisError::AssignmentMissing)
                    })
                })
                .collect(),
        }
    }
}

#[cfg(test)]
mod test_trusted {
    use super::*;
    use ark_bls12_377::Bls12_377;
    use ark_groth16::{
        create_random_proof, generate_random_parameters, prepare_verifying_key, verify_proof,
    };
    use ark_std::test_rng;

    const PT_1: &[u8; 25] = b"crypto means cryptography";

    #[should_panic]
    #[test]
    fn test_in() {
        // Proof setup variables
        let rng = &mut test_rng();
        let target = b"crypto";
        // Verifier
        let verifier = CensorProofSynthesizer::new_verifier(PT_1.len(), target);
        let params = generate_random_parameters::<Bls12_377, _, _>(verifier, rng).unwrap();
        let pvk = prepare_verifying_key(&params.vk);
        // Prover
        let prover = CensorProofSynthesizer::new_prover(PT_1, target);
        // Crashes below in test mode because constraints are not satisfied
        let proof = create_random_proof(prover, &params, rng).unwrap();
        // Compare
        let verif = verify_proof(&pvk, &proof, &[]).unwrap();
        // Crashes below in release mode because assertion fails
        assert!(verif);
    }

    #[test]
    fn test_nin() {
        // Proof setup variables
        let rng = &mut test_rng();
        let target = b"currenc";
        // Verifier
        let verifier = CensorProofSynthesizer::new_verifier(PT_1.len(), target);
        let params = generate_random_parameters::<Bls12_377, _, _>(verifier, rng).unwrap();
        let pvk = prepare_verifying_key(&params.vk);
        // Prover
        let prover = CensorProofSynthesizer::new_prover(PT_1, target);
        let proof = create_random_proof(prover, &params, rng).unwrap();
        // Compare
        let verif = verify_proof(&pvk, &proof, &[]).unwrap();
        assert!(verif);
    }
}

#[cfg(test)]
mod test_universal {
    use super::*;
    use ark_bls12_381::{Bls12_381, Fr};
    use ark_marlin::Marlin;
    use ark_poly::univariate::DensePolynomial;
    use ark_poly_commit::sonic_pc::SonicKZG10;
    use ark_std::test_rng;
    use blake2::Blake2s;

    const PT_1: &[u8; 25] = b"crypto means cryptography";

    type PC = SonicKZG10<Bls12_381, DensePolynomial<Fr>>;

    #[should_panic]
    #[test]
    fn test_in() {
        // Proof setup variables
        let rng = &mut test_rng();
        let target = b"crypto";
        // Setup
        let prover = CensorProofSynthesizer::new_prover(PT_1, target);
        let verifier = CensorProofSynthesizer::new_verifier(PT_1.len(), target);
        let srs = Marlin::<Fr, PC, Blake2s>::universal_setup(1024, 1024, 3 * 1024, rng).unwrap();
        let (pk, vk) = Marlin::<Fr, PC, Blake2s>::index(&srs, verifier).unwrap();
        // Prove
        // Crashes below in test mode because constraints are not satisfied
        let proof = Marlin::<Fr, PC, Blake2s>::prove(&pk, prover, rng).unwrap();
        // Verify
        let verif = Marlin::<Fr, PC, Blake2s>::verify(&vk, &vec![], &proof, rng).unwrap();
        // Crashes below in release mode because assertion fails
        assert!(verif);
    }
    #[test]
    fn test_nin() {
        // Proof setup variables
        let rng = &mut test_rng();
        let target = b"currenc";
        // Setup
        let prover = CensorProofSynthesizer::new_prover(PT_1, target);
        let verifier = CensorProofSynthesizer::new_verifier(PT_1.len(), target);
        let srs = Marlin::<Fr, PC, Blake2s>::universal_setup(1024, 1024, 3 * 1024, rng).unwrap();
        let (pk, vk) = Marlin::<Fr, PC, Blake2s>::index(&srs, verifier).unwrap();
        // Prove
        let proof = Marlin::<Fr, PC, Blake2s>::prove(&pk, prover, rng).unwrap();
        // Verify
        let verif = Marlin::<Fr, PC, Blake2s>::verify(&vk, &vec![], &proof, rng).unwrap();
        assert!(verif);
    }
}
